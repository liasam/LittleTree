package com.sam.lt.common;

import com.sam.lt.common.exception.NodeControlExceiotion;
import com.sam.lt.vo.RetMsg;

public class ExceptionThrower {
	
	public static NodeControlExceiotion createThrowable(Integer code) {
		return new NodeControlExceiotion(RetMsg.getMessage(code));
	}
}
