package com.sam.lt.common.exception;

public class NodeControlExceiotion extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NodeControlExceiotion() {
        super();
    }

    /**
     * @param message
     */
    public NodeControlExceiotion(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public NodeControlExceiotion(Throwable cause) {
        super(cause.getMessage());
    }

    /**
     * 
     * @param cause 异常
     * @param defaultMsg  非业务异常显示提示信息
     */
    public NodeControlExceiotion(Throwable cause, String defaultMsg) {
        super(cause instanceof NodeControlExceiotion ? cause.getMessage() : defaultMsg);
    }
	
}
