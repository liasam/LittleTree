package com.sam.lt.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sam.lt.entity.NodeData;

public interface NodeDataRepository extends JpaRepository<NodeData, Integer>{
	
}
