package com.sam.lt.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sam.lt.entity.Node;

@Repository
public interface NodeRepository extends JpaRepository<Node, Integer> {

}
