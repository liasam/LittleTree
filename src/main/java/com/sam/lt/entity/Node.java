package com.sam.lt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity(name = "tree_node")
public class Node {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//自增主键
	@lombok.Getter @lombok.Setter private Integer id;
	
	@Column(name="node_value")
	@lombok.Getter @lombok.Setter private String value;
	
	@Column(name="parent_Id")
	@lombok.Getter @lombok.Setter private Integer parentId;
	
	public boolean equal(Node other) {
		boolean res = true;
		if(!(this.id != null && this.id == (other.getId())))
			res = false;
		if(!(this.value != null && this.value.equals(other.getValue())))
			res = false;
		if(!(this.parentId != null && this.parentId == (other.getParentId())))
			res = false;
		
		return res;
	}
}
