package com.sam.lt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * this bean is map to database table node_carry_data
 * @author 58412
 *
 */
@Entity(name = "NODE_CARRY_DATA")
public class NodeData {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//自增主键
	@lombok.Getter @lombok.Setter private Integer id;
	
	@Column(name="node_id")
	@lombok.Getter @lombok.Setter private Integer nodeId;
	
	@Column(name="key")
	@lombok.Getter @lombok.Setter private String key;
	
	@Column(name="value")
	@lombok.Getter @lombok.Setter private String value;
}
