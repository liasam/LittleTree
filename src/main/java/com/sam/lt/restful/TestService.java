package com.sam.lt.restful;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestService {
	
	@RequestMapping(value = "/function1", produces = "application/json")
	public String[] getArticle() {
		return new String[] {"1","2","3"};
	}
}
