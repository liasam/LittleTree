package com.sam.lt.service;

import java.util.List;

import com.sam.lt.entity.NodeData;
import com.sam.lt.vo.TreeNodeDataVo;
import com.sam.lt.vo.TreeNodeVo;

public interface NodeDataService {
	void batchSave(List<NodeData> list);
	
	void batchSaveOrUpdate(Integer nodeId,List<NodeData> list);
	
	TreeNodeDataVo getNodeData(Integer nodeId);
	
	void hangUpTree(TreeNodeVo branch);
}
