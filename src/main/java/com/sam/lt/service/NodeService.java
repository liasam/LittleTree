package com.sam.lt.service;

import java.util.List;

import com.sam.lt.entity.Node;
import com.sam.lt.vo.TreeNodeVo;

public interface NodeService {
	Node createNode(String value,Integer parent);
	
	TreeNodeVo viewTree(Integer NodeId);
	
	void removeNode(Integer NodeId);
	
	TreeNodeVo getNode(Integer NodeId);
	
	List<TreeNodeVo> ListAllTree();
	
	TreeNodeVo getTreeByName(String name);
	
	TreeNodeVo getTreeByName(String name, Integer level);
	
	void updateOrCreateTree(TreeNodeVo root);
}
