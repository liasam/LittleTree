package com.sam.lt.vo;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author 58412
 *
 */
public class RetMsg {
	public static final Integer SUCCESS = 0;
	public static final Integer ERROR = -1;
	public static final String UNKNOWN_ERROR = "unknown error";
	public static final Map<Integer, String> messageMap = new HashMap<>();
	
	
	private static final Integer ZERO_CODE_OF_EXCEPTION = 1024;
	public static final Integer NODE_NOT_FOUND_EXCEPTION = ZERO_CODE_OF_EXCEPTION+1;
	
	
	static {
		messageMap.put(SUCCESS, "success");
		messageMap.put(ERROR, "error");
		
		messageMap.put(NODE_NOT_FOUND_EXCEPTION, "Node not found!");
	}
	
	@lombok.Getter @lombok.Setter private Integer code;
	@lombok.Getter @lombok.Setter private String message;
	
	public static String getMessage(Integer code) {
		return messageMap.get(code);
	}
	
	public static RetMsg getResult(Integer code) {
		RetMsg o = new RetMsg();
		o.code = code;
		o.message = messageMap.get(code);
		if(o.message == null)
			o.message = UNKNOWN_ERROR;
		return o;
	}
}
