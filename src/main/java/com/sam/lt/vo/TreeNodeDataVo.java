package com.sam.lt.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sam.lt.entity.NodeData;

public class TreeNodeDataVo extends NodeData{
	@lombok.Getter @lombok.Setter private List<Map<String,String>> data;
	
	public List<NodeData> spliteToNodeData() {
		List<NodeData> list = new ArrayList<>();
		if(this.getNodeId() != null && data.size() > 0) {
			for(Map<String,String> item : data) { // for1
				NodeData nodeData = new NodeData();
				nodeData.setNodeId(this.getNodeId());
				for(String key :item.keySet()) {  // for2 one item
					nodeData.setKey(key);
					nodeData.setValue(item.get(key));
					break; //just only one
				}// for2 
				
				list.add(nodeData);
			} // for1
			
		}
		return list;
	}
	
	/**
	 * create a TreeNodeDataVo dependence on some NodeDatas
	 * @param list
	 * @return
	 */
	public static TreeNodeDataVo convertFromNodeData(List<NodeData> list) {
		if(list!=null && list.size()>0) {
			TreeNodeDataVo vo = new TreeNodeDataVo();
			vo.setNodeId(list.get(0).getNodeId());
			List<Map<String,String>> data = new ArrayList<>();
			for(int i=0;i<list.size();i++) {
				Map<String,String> item = new HashMap<String,String>();
				item.put(list.get(i).getKey(), list.get(i).getValue());
				data.add(item);
			}
			vo.setData(data);
			
			return vo;
		}
		return null;
		
	}
}
