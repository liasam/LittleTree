package com.sam.lt.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;

import com.sam.lt.entity.Node;

public class TreeNodeVo extends Node implements Cloneable{
	
	@lombok.Getter @lombok.Setter private String text;
	
	@lombok.Getter private List<TreeNodeVo> children;
	
	@lombok.Getter @lombok.Setter private Map<String,Object> data;
	
	public TreeNodeVo() {
		super();
		data = new HashMap<>();
	}
	
	public static TreeNodeVo fromEntity(Node node) {
		TreeNodeVo vo = new TreeNodeVo();
		BeanUtils.copyProperties(node, vo);
		vo.setText(node.getValue());
		return vo;
	}
	
	public static List<TreeNodeVo> fromEntity(List<Node> nodes) {
		if(nodes == null)
			return null;
		
		List<TreeNodeVo> list = new ArrayList<>();
		for(Node node : nodes) {
			TreeNodeVo vo = fromEntity(node);
			list.add(vo);
		}
		
		return list;
	}
	
	public Object clone() {
        Object obj=null;
        //调用Object类的clone方法，返回一个Object实例
        try {
            obj= super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return obj;
    }
	
	public Node cloneToBeNode() {
		Node node = new Node();
		BeanUtils.copyProperties(this, node);
		return node;
	}
	
	public void addChild(TreeNodeVo nodeVo) {
		if(children == null)
			children = new ArrayList<>();
		
		children.add(nodeVo);
	}
	
	public void addChild(List<TreeNodeVo> list) {
		if(children == null)
			children = new ArrayList<>();
		
		children.addAll(list);
	}
	
	public void putInData(String key,Object item) {
		data.put(key, item);
	}
}
