create table if not exists tree_node (
	id int  PRIMARY KEY AUTO_INCREMENT,
	node_value varchar(255),
	parent_Id int not null
);
drop index if exists tree_node_id1;
create index tree_node_id1 on tree_node(parent_Id);

create table if not exists NODE_CARRY_DATA (
	id int  PRIMARY KEY AUTO_INCREMENT,
	node_id int,
	key varchar(55),
	value varchar(1024)
);
drop index if exists NODE_CARRY_DATA_IDX1;
create index NODE_CARRY_DATA_IDX1 on NODE_CARRY_DATA(node_id);