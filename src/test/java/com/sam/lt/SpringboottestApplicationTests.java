package com.sam.lt;

import java.util.List;

import javax.annotation.Resource;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import org.springframework.transaction.annotation.Transactional;

import com.sam.lt.dao.NodeDataRepository;
import com.sam.lt.dao.NodeRepository;
import com.sam.lt.entity.Node;
import com.sam.lt.entity.NodeData;
import com.sam.lt.service.NodeService;
import com.sam.lt.vo.TreeNodeVo;

@SpringBootTest
public class SpringboottestApplicationTests {
	@Resource
	NodeRepository nodeRepository;
	@Resource
	NodeDataRepository nodeDataRepository;
	@Autowired
	NodeService nodeService;
	
	@Test
    void contextLoads() {

    }
	
	@Test
	@Commit
    void testBuildATree() {
//		nodeRepository.deleteAll();
//		
//		for(int t=0;t<3;t++) {
//			Node tree = new Node();
//			tree.setId(null);
//			tree.setParentId(0);
//			tree.setValue("tree"+t);
//			Node res = nodeRepository.save(tree);
//			
//			for(int i=0;i<10;i++ ) {
//				Node node = new Node();
//				node.setId(null);
//				node.setValue("node"+i);
//				node.setParentId(res.getId());
//				nodeRepository.save(node);
//			}
//		}
    }
	
	@Test
    void testQueryNodeData() {
		List<Node> list = nodeRepository.findAll();
		System.out.println("Node num:"+list.size());
		for(Node node : list) {
			System.out.println(String.format("|%d|%s|%d|", 
					node.getId(),node.getValue(),node.getParentId()));
			
		}
	}
	
	@Test
    void testViewTree() {
		List<TreeNodeVo> list = nodeService.ListAllTree();
		System.out.println("tree num:"+list.size());
		for(Node node : list) {
			System.out.println(String.format("|%d|%s|%d|", 
					node.getId(),node.getValue(),node.getParentId()));
			
		}
	}
	
	@Test
    void testInsertNodeData() {
		NodeData nodeData = new NodeData();
		nodeData.setId(null);
		nodeData.setKey("testKey");
		nodeData.setNodeId(1);
		nodeData.setValue("testValue");
		nodeDataRepository.save(nodeData);
	}
}
